package ru.trippel.tm.context;

import ru.trippel.tm.command.*;
import ru.trippel.tm.repository.ProjectRepository;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.service.ProjectService;
import ru.trippel.tm.service.TaskService;
import ru.trippel.tm.view.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class Bootstrap {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    private void execute(final String command) {
        if (command == null) return;
        if (command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        try {
            abstractCommand.execute();
        } catch (Exception e) {
            BootstrapView.printError();
        }
    }

    public void start() throws Exception {
        BootstrapView.printWelcome();
        String command="";
        init();
        while (true) {
            command = KeyboardView.read();
            if ("EXIT".equals(command)) {
                end();
            }
            if (command.isEmpty()) {
                BootstrapView.printError();
                continue;
            }
            execute(command);
        }
    }

    public void init() {
        commands.put("HELP", new HelpCommand(this));
        commands.put("PROJECT_CREATE", new ProjectCreateCommand(this));
        commands.put("PROJECT_VIEW", new ProjectViewCommand(this));
        commands.put("PROJECT_EDIT", new ProjectEditCommand(this));
        commands.put("PROJECT_REMOVE", new ProjectRemoveCommand(this));
        commands.put("PROJECT_ATTACHTASK", new ProjectAttachTaskCommand(this));
        commands.put("PROJECT_VIEWTASK", new ProjectViewTasksCommand(this));
        commands.put("TASK_CREATE", new TaskCreateCommand(this));
        commands.put("TASK_VIEW", new TasktViewCommand(this));
        commands.put("TASK_EDIT", new TaskEditCommand(this));
        commands.put("TASK_REMOVE", new TaskRemoveCommand(this));
        commands.put("EXIT", new ExitCommand(this));
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    private void end() {
        BootstrapView.printGoodbye();
        System.exit(0);
    }

}
