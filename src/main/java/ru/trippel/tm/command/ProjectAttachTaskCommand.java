package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class ProjectAttachTaskCommand extends AbstractCommand {

    public ProjectAttachTaskCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_ATTACHTASK";
    }

    @Override
    public String getDescription() {
        return "Attach Task to Project";
    }

    @Override
    public void execute() throws Exception {
        String projectId;
        String taskId;
        List<Task> taskList = taskService.findAll();
        List<Project> projectList = projectService.findAll();
        int projectNum = -1;
        int taskNum = -1;
        Project project;
        Task taskTemp;
        Task task;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            project = projectList.get(i);
            System.out.println(i + 1 + ". " + project.getProjectId() + " - " + project.getName());
        }
        System.out.println("Enter a project number");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        projectId = projectList.get(projectNum).getProjectId();
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            taskTemp = taskList.get(i);
            System.out.println(i + 1 + ". " + taskTemp.getTaskId() + " - " + taskTemp.getName());
        }
        System.out.println("Enter a project number");
        taskNum +=  Integer.parseInt(KeyboardView.read());
        taskId = taskList.get(taskNum).getTaskId();
        task = taskService.findOne(taskId);
        task.setProjectId(projectId);
    }

}
