package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "TASK_REMOVE";
    }

    @Override
    public String getDescription() {
        return "Remove project";
    }

    @Override
    public void execute() throws Exception {
        String id;
        List<Task> taskList = taskService.findAll();
        int taskNum = -1;
        Task task;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            System.out.println(i+1 + ". " + task.getTaskId() + " - " + task.getName());
        }
        System.out.println("Enter a task number to Edit:");
        taskNum +=  Integer.parseInt(KeyboardView.read());
        id = taskList.get(taskNum).getTaskId();
        taskService.remove(id);
    }

}
