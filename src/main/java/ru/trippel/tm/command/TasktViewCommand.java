package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import java.util.List;

public class TasktViewCommand extends AbstractCommand {

    public TasktViewCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "TASK_VIEW";
    }

    @Override
    public String getDescription() {
        return "Show all project";
    }

    @Override
    public void execute() throws Exception {
        List<Task> taskList = taskService.findAll();
        Task task;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            System.out.println(i+1 + ". " + task.getTaskId() + " - " + task.getName());
        }
    }

}
