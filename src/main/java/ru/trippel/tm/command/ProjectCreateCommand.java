package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.view.KeyboardView;

public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_CREATE";
    }

    @Override
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() throws Exception {
        Project project = new Project();
        System.out.println("Enter project name.");
        String name = KeyboardView.read();
        project.setName(name);
        projectService.persist(project);
        System.out.println("Project \"" + project.getProjectId() + " - " + project.getName() + "\" added! - ");
    }

}
