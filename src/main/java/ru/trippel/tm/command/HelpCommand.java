package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;

public final class HelpCommand extends AbstractCommand {

    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "HELP";
    }

    @Override
    public String getDescription() {
        return "List commands";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Command list:");
        for (final AbstractCommand command:
        bootstrap.getCommands()) {
            System.out.println(command.getNameCommand() + ": "
                    + command.getDescription());
        }
    }

}
