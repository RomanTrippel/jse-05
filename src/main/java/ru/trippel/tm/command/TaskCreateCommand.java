package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "TASK_CREATE";
    }

    @Override
    public String getDescription() {
        return "Create new Task";
    }

    @Override
    public void execute() throws Exception {
        Task task = new Task();
        System.out.println("Enter project name.");
        String name = KeyboardView.read();
        task.setName(name);
        taskService.persist(task);
        System.out.println("Task \"" + task.getTaskId() + " - " + task.getName() + "\" added!");
    }

}
