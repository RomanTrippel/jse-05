package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_REMOVE";
    }

    @Override
    public String getDescription() {
        return "Remove project";
    }

    @Override
    public void execute() throws Exception {
        String projectId;
        List<Task> taskList = taskService.findAll();
        List<Project> projectList = projectService.findAll();
        int projectNum = -1;
        Project project;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            project = projectList.get(i);
            System.out.println(i + 1 + ". " + project.getProjectId() + " - " + project.getName());
        }
        System.out.println("Enter a project number to Remove");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        projectId = projectList.get(projectNum).getProjectId();
        projectService.remove(projectId);
        for (int i = 0; i < taskList.size() ; i++) {
            if (taskList.get(i).getProjectId().equals(projectId)) {
                taskService.remove(taskList.get(i).getTaskId());
            }
        }
    }

}
