package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import java.util.List;

public class ProjectViewCommand extends AbstractCommand {

    public ProjectViewCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_VIEW";
    }

    @Override
    public String getDescription() {
        return "Show all project";
    }

    @Override
    public void execute() throws Exception {
        List<Project> projectList = projectService.findAll();
        Project project;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            project = projectList.get(i);
            System.out.println(i + 1 + ". " + project.getProjectId() + " - " + project.getName());
        }
    }

}