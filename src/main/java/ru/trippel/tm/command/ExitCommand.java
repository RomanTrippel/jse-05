package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;

public final class ExitCommand extends AbstractCommand {

    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "EXIT";
    }

    @Override
    public String getDescription() {
        return "Exit the application";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Invalid request, try again");
        System.exit(0);
    }

}
