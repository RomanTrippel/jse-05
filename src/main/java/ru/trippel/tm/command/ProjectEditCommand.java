package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_EDIT";
    }

    @Override
    public String getDescription() {
        return "Edit project";
    }

    @Override
    public void execute() throws Exception {
        List<Project> projectList = projectService.findAll();
        Project project;
        String id;
        String newName;
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            project = projectList.get(i);
            System.out.println(i + 1 + ". " + project.getProjectId() + " - " + project.getName());
        }
        System.out.println("Enter a project number to Edit:");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        id = projectList.get(projectNum).getProjectId();
        System.out.println("Enter new Name:");
        newName = KeyboardView.read();
        project = projectService.findOne(id);
        project.setName(newName);
        projectService.merge(project);
    }

}
