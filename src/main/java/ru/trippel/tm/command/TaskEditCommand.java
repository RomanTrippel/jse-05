package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class TaskEditCommand extends AbstractCommand {

    public TaskEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "TASK_EDIT";
    }

    @Override
    public String getDescription() {
        return "Edit project";
    }

    @Override
    public void execute() throws Exception {
        List<Task> taskList = taskService.findAll();
        Task task;
        String id;
        String newName;
        int taskNum = -1;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            System.out.println(i + 1 + ". " + task.getTaskId() + " - " + task.getName());
        }
        System.out.println("Enter a task number to Edit:");
        taskNum +=  Integer.parseInt(KeyboardView.read());
        id = taskList.get(taskNum).getTaskId();
        System.out.println("Enter new Name:");
        newName = KeyboardView.read();
        task = taskService.findOne(id);
        task.setName(newName);
        taskService.merge(task);
    }

}
