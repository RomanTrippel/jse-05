package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.service.ProjectService;
import ru.trippel.tm.service.TaskService;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    protected ProjectService projectService;

    protected TaskService taskService;

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        projectService = bootstrap.getProjectService();
        taskService = bootstrap.getTaskService();
    }

    public abstract String getNameCommand();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

}

