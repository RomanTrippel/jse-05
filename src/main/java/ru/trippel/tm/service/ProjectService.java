package ru.trippel.tm.service;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.repository.ProjectRepository;
import ru.trippel.tm.view.KeyboardView;

import java.io.IOException;
import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findOne(String id) throws NullPointerException {
        if (id == null) throw new NullPointerException();
        if (id.isEmpty()) return null;
        return projectRepository.findOne(id);
    }

    public Project persist(Project project) throws NullPointerException {
        if (project == null) throw new NullPointerException();
        String id = project.getProjectId();
        if (id.isEmpty()) return null;
        return projectRepository.persist(project);
    }

    public Project merge(Project project) throws NullPointerException {
        if (project == null) throw new NullPointerException();
        String id = project.getProjectId();
        if (id.isEmpty()) return null;
        return projectRepository.merge(project);
    }

    public Project remove(String id) throws NullPointerException {
        if (id == null) throw new NullPointerException();
        if (id.isEmpty()) return null;
        return projectRepository.remove(id);
    }

}
